#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_TENTATIVAS 6
#define TAMANHO_PALAVRA 20

// Fun��o para imprimir a forca com base no n�mero de tentativas restantes
void imprimirForca(int tentativasRestantes) {
    printf("\n");
    printf("  +---+\n");
    printf("  |   |\n");

    if (tentativasRestantes < MAX_TENTATIVAS)
        printf("  |   O\n");
    else
        printf("  |\n");

    if (tentativasRestantes < MAX_TENTATIVAS - 1)
        printf("  |   |\n");
    else if (tentativasRestantes < MAX_TENTATIVAS)
        printf("  |  /|\n");
    else
        printf("  |\n");

    if (tentativasRestantes < MAX_TENTATIVAS - 3)
        printf("  |  / ");
    else if (tentativasRestantes < MAX_TENTATIVAS - 2)
        printf("  |  /|");
    else if (tentativasRestantes < MAX_TENTATIVAS - 1)
        printf("  |  /|\\");
    else if (tentativasRestantes < MAX_TENTATIVAS)
        printf("  |  /|\\");
    else
        printf("  |\n");

    printf("  |\n");
    printf("======\n\n");
}

// Fun��o para verificar se uma letra est� presente na palavra
int verificarLetra(char letra, char palavra[], char palavraEscondida[]) {
    int i, encontrou = 0;
    for (i = 0; i < strlen(palavra); i++) {
        if (tolower(letra) == tolower(palavra[i])) {
            palavraEscondida[i] = palavra[i];
            encontrou = 1;
        }
    }
    return encontrou;
}

int main() {
    char palavra[TAMANHO_PALAVRA];
    char palavraEscondida[TAMANHO_PALAVRA];
    char letra;
    int tentativasRestantes = MAX_TENTATIVAS;
    int palavraRevelada = 0;

    // Solicita ao jogador a palavra a ser adivinhada
    printf("Digite a palavra para o jogo da forca (sem espa�os, com no m�ximo %d caracteres): ", TAMANHO_PALAVRA - 1);
    scanf("%s", palavra);

    // Inicializa a palavra escondida com "-"
    memset(palavraEscondida, '-', strlen(palavra));
    palavraEscondida[strlen(palavra)] = '\0';

    // Loop principal do jogo
    while (tentativasRestantes > 0 && !palavraRevelada) {
        printf("\nPalavra: %s\n", palavraEscondida);
        printf("Tentativas restantes: %d\n", tentativasRestantes);

        printf("Digite uma letra: ");
        scanf(" %c", &letra);

        // Verifica se a letra j� foi tentada
        if (strchr(palavraEscondida, tolower(letra)) != NULL) {
            printf("Voc� j� tentou esta letra. Tente outra.\n");
            continue;
        }

        // Verifica se a letra est� presente na palavra
        if (verificarLetra(letra, palavra, palavraEscondida)) {
            printf("Letra correta!\n");

            // Verifica se todas as letras foram adivinhadas
            if (strcmp(palavra, palavraEscondida) == 0) {
                palavraRevelada = 1;
            }
        } else {
            printf("Letra incorreta.\n");
            tentativasRestantes--;
        }

        // Imprime a forca
        imprimirForca(tentativasRestantes);
    }

    // Verifica se o jogador venceu ou perdeu
    if (palavraRevelada) {
        printf("\nParab�ns! Voc� adivinhou a palavra \"%s\"!\n", palavra);
    } else {
        printf("\nVoc� perdeu! A palavra era \"%s\".\n", palavra);
    }

    return 0;
}
